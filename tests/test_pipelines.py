import pytest
import logging
from pyspark.sql import SparkSession
from ..weekly_sales_job.pipelines import (sales_pipeline,
                                          calendar_pipeline,
                                          product_pipeline,
                                          store_pipeline)


def quiet_py4j():
    """Suppress spark logging for the test context"""
    logger = logging.getLogger('py4j')
    logger.setLevel(logging.WARN)


@pytest.fixture(scope="session")
def spark_session(request):
    """Fixture for creating Spark context"""
    session = (
        SparkSession.builder
        .appName('pytest-pyspark-local-testing')
        .master('local[*]')
        .getOrCreate()
    )
    request.addfinalizer(lambda: session.stop())

    quiet_py4j()
    return session


# def test_sales_pipeline(spark_session):
#     assert True


def test_calendar_pipeline(spark_session):
    calendar = spark_session.createDataFrame(data=[(420, 1, 2020), (421, 2, 2020)],
                                             schema=['datekey', 'dayofmonth', 'year'])

    result = spark_session.createDataFrame(data=[(420, 1, 2020, 1), (421, 2, 2020, 1)],
                                           schema=['dateId', 'dayofmonth', 'year', 'weekofyear'])

    assert calendar_pipeline(calendar, spark_session).collect() == result.collect()


def test_product_pipeline(spark_session):
    product = spark_session.createDataFrame(data=[(1, 'ASDF'), (2, 'QWER')],
                                            schema=['productid', 'product'])
    result = spark_session.createDataFrame(data=[(1, 'ASDF'), (2, 'QWER')],
                                           schema=['productId', 'product'])
    assert product_pipeline(product).collect() == result.collect()


def test_store_pipeline(spark_session):
    store = spark_session.createDataFrame(data=[(1, 'ASDF'), (2, 'QWER')],
                                          schema=['storeid', 'store'])
    result = spark_session.createDataFrame(data=[(1, 'ASDF'), (2, 'QWER')],
                                           schema=['productId', 'store'])
    assert store_pipeline(store).collect() == result.collect()
