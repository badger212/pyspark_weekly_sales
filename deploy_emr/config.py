AWS_CREDENTIALS_PROFILE = 'ivan'
SPARK_JOB_PATH = 's3://iby-utils-pyspark-test/job/'

##############
# Job config #
##############

ENTRYPOINT = 'main.py'
JOB_STEPS = [
    {
        'Name': 'Setup Debugging',
        'ActionOnFailure': 'TERMINATE_CLUSTER',
        'HadoopJarStep': {
            'Jar': 'command-runner.jar',
            'Args': ['state-pusher-script']
        }
    },
    {
        'Name': 'Copy Job',
        'ActionOnFailure': 'TERMINATE_CLUSTER',
        'HadoopJarStep': {
            'Jar': 'command-runner.jar',
            'Args': ['aws', 's3', 'cp', SPARK_JOB_PATH,
                     '/home/hadoop/', '--recursive']
        }
    },
    {
        'Name': 'Run Spark',
        'ActionOnFailure': 'TERMINATE_CLUSTER',
        'HadoopJarStep': {
            'Jar': 'command-runner.jar',
            'Args': [
                'spark-submit',
                '--master', 'yarn',
                '--deploy-mode', 'client',
                f'/home/hadoop/{ENTRYPOINT}']
        }
    }
]


def change_action_on_failure(step):
    step['ActionOnFailure'] = 'CONTINUE'
    return step


PERSISTENT_CLUSTER_JOB_STEPS = [change_action_on_failure(step) for step in JOB_STEPS[1:]]

################
# Infra config #
################

# persistent cluster
PERSISTENT_CLUSTER_ID = None

# software
RELEASE_LABEL = 'emr-5.26.0'
APPLICATIONS = [{'Name': 'Spark'}]
CONFIGURATIONS = [  # configure job to be run by python 3
    {
        "Classification": "spark-env",
        "Configurations": [
            {
                "Classification": "export",
                "Properties": {
                   "PYSPARK_PYTHON": "/usr/bin/python3"
                }
            }
        ]
    }
]

# instances
EC2_SUBNET_ID = 'subnet-0c0bc9529eb8dd4f3'
MASTER_INSTANCE_COUNT = 1
MASTER_INSTANCE_TYPE = 'm4.large'
CORE_INSTANCE_COUNT = 2
CORE_INSTANCE_TYPE = 'm4.large'
JOB_FLOW_ROLE = 'EMR_EC2_DefaultRole'
SERVICE_ROLE = 'EMR_DefaultRole'
INSTANCES = {
    'Ec2SubnetId': EC2_SUBNET_ID,
    'InstanceGroups': [
        {
            'InstanceCount': MASTER_INSTANCE_COUNT,
            'InstanceType': MASTER_INSTANCE_TYPE,
            'InstanceRole': 'MASTER',
            'Market': 'SPOT',
            'Name': 'master',
        },
        {
            'InstanceCount': CORE_INSTANCE_COUNT,
            'InstanceType': CORE_INSTANCE_TYPE,
            'InstanceRole': 'CORE',
            'Market': 'SPOT',
            'Name': 'core',
        }
    ],
    'KeepJobFlowAliveWhenNoSteps': False,
    'TerminationProtected': False,
}

# logs
LOG_URI = 's3://aws-logs-727443295739-eu-west-1/elasticmapreduce'

# Glue
GLUE_SERVICE_ROLE = 'GlueServiceRole'
