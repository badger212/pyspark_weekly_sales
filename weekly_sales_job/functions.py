import logging
from typing import Callable

from pyspark.sql import DataFrame, SparkSession
import pyspark.sql.functions as F
from pyspark.sql.types import StringType

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

id_function = F.udf(lambda *cols: '_'.join([c.upper() for c in cols]), returnType=StringType())


def get_spark_session(spark_mode='yarn') -> SparkSession:
    logger.info(f"Building Spark session. Mode: {spark_mode}")
    return (
        SparkSession.builder
        .appName('WeeklySales')
        .master(spark_mode)
        .getOrCreate()
    )


def csv_reader(session: SparkSession, path: str) -> DataFrame:
    return (session.read
            .format('csv')
            .option("header", True)
            .load(path)
            )


def load_and_preprocess(session: SparkSession, path: str, reader: Callable, pipeline: Callable) -> DataFrame:
    df = reader(session, path)
    return pipeline(df, session)
