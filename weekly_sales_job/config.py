# Paths

# local
# ROOT_BUCKET = "data"
# SOURCE_PATH = "/source"
# SALES_PATH = ROOT_BUCKET + SOURCE_PATH + "/sales.csv"
# CALENDAR_PATH = ROOT_BUCKET + SOURCE_PATH + "/calendar.csv"
# PRODUCT_PATH = ROOT_BUCKET + SOURCE_PATH + "/product.csv"
# STORE_PATH = ROOT_BUCKET + SOURCE_PATH + "/store.csv"
# OUTPUT_PATH = ROOT_BUCKET + "/consumption"

# aws
ROOT_BUCKET = "s3://iby-utils-pyspark-test"
SOURCE_PATH = "/source"
SALES_PATH = ROOT_BUCKET + SOURCE_PATH + "/sales.csv"
CALENDAR_PATH = ROOT_BUCKET + SOURCE_PATH + "/calendar.csv"
PRODUCT_PATH = ROOT_BUCKET + SOURCE_PATH + "/product.csv"
STORE_PATH = ROOT_BUCKET + SOURCE_PATH + "/store.csv"
OUTPUT_PATH = ROOT_BUCKET + "/consumption"

# Processing settings
# GROUP_COLUMNS = ['dateId', 'storeId', 'productId']
GROUP_COLUMNS = ['datecalendaryear', 'weekofyear', 'channel', 'division', 'gender', 'category']
AGG_MAPPING = {
    'netSales': 'sum',
    'salesUnits': 'sum'
}
UNIQUE_ID_COLUMNS = ['datecalendaryear', 'channel', 'division', 'gender', 'category']
OUTPUT_COLUMNS = ['uniqueId', 'weekofyear'] + [f"{val}({key})" for key, val in AGG_MAPPING.items()]
