from typing import Optional
from datetime import datetime, timedelta

from pyspark.sql import SparkSession, DataFrame, Window
from pyspark.sql import functions as F


def sales_pipeline(sales: DataFrame, session: Optional[SparkSession] = None) -> DataFrame:
    return (
        sales
        .na.drop()
    )


def calendar_pipeline(calendar: DataFrame, session: SparkSession) -> DataFrame:
    """ Add week of year to calendar dataframe. It is assumed that calendar is an ordered dataframe of 365 days """
    date = [(datetime(2018, 1, 1) + n * timedelta(days=1)).strftime("%Y-%m-%d") for n in range(365)]
    week_of_year = (
        session
        .createDataFrame([(d,) for d in date], ['date'])
        .withColumn('weekofyear', F.weekofyear('date'))
        .drop('date')
    )
    return (
        calendar
        .withColumn("row_id", F.row_number().over(Window.orderBy(F.monotonically_increasing_id())))
        .join(
            week_of_year
            .withColumn("row_id", F.row_number().over(Window.orderBy(F.monotonically_increasing_id()))),
            "row_id"
        )
        .drop("row_id")
        .withColumnRenamed('datekey', 'dateId')
    )


def product_pipeline(product: DataFrame, session: Optional[SparkSession] = None) -> DataFrame:
    return (
        product
        .na.drop()
        .withColumnRenamed('productid', 'productId')
    )


def store_pipeline(store: DataFrame, session: Optional[SparkSession] = None) -> DataFrame:
    return (
        store
        .na.drop()
        .withColumnRenamed('storeid', 'storeId')
    )