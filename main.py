import argparse

from weekly_sales_job import config
from weekly_sales_job.functions import get_spark_session, csv_reader, load_and_preprocess, id_function
from weekly_sales_job.pipelines import sales_pipeline, calendar_pipeline, product_pipeline, store_pipeline


parser = argparse.ArgumentParser(description='Launch weekly sales job')
parser.add_argument('-m', '--mode', metavar='mode', type=str, default='yarn',
                    help='Spark mode. Choose local or yarn.')
args = parser.parse_args()
assert args.mode in ['local', 'yarn']

session = get_spark_session(spark_mode=args.mode)

sales = load_and_preprocess(session, config.SALES_PATH, csv_reader, sales_pipeline)
calendar = load_and_preprocess(session, config.CALENDAR_PATH, csv_reader, calendar_pipeline)
product = load_and_preprocess(session, config.PRODUCT_PATH, csv_reader, product_pipeline)
store = load_and_preprocess(session, config.STORE_PATH, csv_reader, store_pipeline)

weekly_sales = (
    sales
    .join(calendar, 'dateId')
    .join(product, 'productId')
    .join(store, 'storeId')
    .groupBy(config.GROUP_COLUMNS)
    .agg(config.AGG_MAPPING)
    .withColumn('uniqueId', id_function(*config.UNIQUE_ID_COLUMNS))
    .select(config.OUTPUT_COLUMNS)
)

weekly_sales.write.partitionBy('uniqueId').mode("append").json(config.OUTPUT_PATH)
