##  Weekly sales job

### Project structure
```
pyspark_weekly_sales/
├── weekly_sales_job/
│   ├── config.py     // paths, job parameters
│   ├── pipelines.py  // preprocessing pipelines for individual dataframes
│   └── functions.py  // higher level functions
├── tests/
|   └── test_pipelines.py
├─- main.py  // job entry point
└── requirements.txt
```

### "Disclaimer"

The output format is not exactly matching the requested in the assignment. Instead, the output json files are of format:

```
[
    {
        "week": 1,
        "sales: 111,
        "units": 111
    }, ...
]
```
The name of the partition is the unique key. The reason the format doesn't match is that I didn't have enough time 
to figure out how exactly to do it in PySpark. The quick way I thought of would be, for example, running a separate 
script after the Spark job is done that would traverse the partitions and reformat the jsons.

### Run locally

You need to update `config.py` with paths to source data and an output path. After that, run:

```
python -m venv venv && source venv/bin/activate
pip install wheel
pip install -r requirements.txt
python main.py --mode local
```

To run tests, make sure you create the environment with **python<=3.7** and run:

```
pip install -r dev-requirements.txt
pytest
```

### Deploy

There is a `boto3` deploy script included. It will work given you have access to AWS and have respective permissions.
You need to tweak the config to:

1. Fill in correct paths in `weekly_sales_job/config.py`
2. Provide correct AWS credentials profile, job path, service role, instance profile and subnet ID

Install dev requirements and run:

```
python deploy.py
```


### TODO
1. Script to  reformat jsons
2. Write setup.py to get rid of relative imports in tests
