from typing import Dict, List, Tuple
from datetime import datetime
import logging
from time import sleep
import argparse

import boto3
from boto3 import client as Client

from deploy_emr import config

logging.basicConfig(level=logging.INFO)


def start_emr_job(client: Client) -> str:
    response = client.run_job_flow(
        Name=f'weekly-sales-job-{datetime.now().strftime("%Y-%m-%d-%H-%M-%S")}',
        VisibleToAllUsers=True,
        Applications=config.APPLICATIONS,
        Configurations=config.CONFIGURATIONS,
        LogUri=config.LOG_URI,
        Instances=config.INSTANCES,
        ReleaseLabel=config.RELEASE_LABEL,
        JobFlowRole=config.JOB_FLOW_ROLE,
        ServiceRole=config.SERVICE_ROLE,
        Steps=config.JOB_STEPS,
        ScaleDownBehavior='TERMINATE_AT_TASK_COMPLETION',
    )
    return response['JobFlowId']


def post_emr_job(client: Client) -> Tuple[str, List[str]]:
    response = client.add_job_steps(
        JobFlowId=config.PERSISTENT_CLUSTER_ID,
        Steps=config.PERSISTENT_CLUSTER_JOB_STEPS
    )
    return config.PERSISTENT_CLUSTER_ID, response['StepIds']


def get_status(client: Client, cluster_id: str) -> Dict:
    response = client.describe_cluster(ClusterId=cluster_id)
    return response['Cluster']['Status']


def get_job_steps(client: Client) -> Tuple[List[Dict], List[Dict], List[Dict]]:
    steps = client.list_steps(ClusterId=cluster_id, StepIds=step_ids)['Steps']
    failed_steps = [step for step in steps if step['Status']['State'] not in ['PENDING', 'RUNNING', 'COMPLETED']]
    completed_steps = [step for step in steps if step['Statu']['State'] == 'COMPLETED']
    return steps, failed_steps, completed_steps


def log_step(step: Dict) -> str:
    return f"Step {step['Name']}, status: {step['Status']['State']}, " \
           f"message: {step['Status']['StateChangeReason']['Message']}"


def start_glue_job(client: Client) -> Tuple[str, str]:
    response = client.create_job(
        Name=f'weekly-sales-job-{datetime.now().strftime("%Y-%m-%d-%H-%M-%S")}',
        Description='',
        LogUri=config.LOG_URI,
        Role=config.GLUE_SERVICE_ROLE,
        Command={
            'Name': 'glueetl',
            'ScriptLocation': config.SPARK_JOB_PATH + 'main.py',
            'PythonVersion': '3',
        },
        GlueVersion='2.0',
        Timeout=2,
        # NumberOfWorkers=2
    )
    job_name = response['Name']
    logging.info(f"Job {job_name} created")
    response = client.start_job_run(
        JobName=job_name
    )
    return job_name, response['JobRunId']


def get_glue_job_status(client: Client, job_name: str, job_run_id: str) -> Tuple[str, str]:
    response = client.get_job_run(
        JobName=job_name,
        RunId=job_run_id
    )['JobRun']
    status = response['JobRunState']
    message = response.get('ErrorMessage')
    return status, message


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Deploy weekly sales job')
    parser.add_argument('-m', '--mode', metavar='mode', type=str, default='standalone',
                        help='Define whether to start a new cluster or post a job to a running cluster\n'
                             'Possible values are "standalone" or "persistent"')
    args = parser.parse_args()
    print(args.mode)
    assert args.mode in ['standalone', 'persistent', 'glue']

    session = boto3.Session(profile_name=config.AWS_CREDENTIALS_PROFILE, region_name='eu-west-1')
    if args.mode == 'glue':
        client = session.client(service_name='glue')
        job_name, job_run_id = start_glue_job(client)
        start = datetime.now()
        status = ''
        while status not in ['TIMEOUT']:
            status, message = get_glue_job_status(client, job_name, job_run_id)
            logging.info(f"Job status: {status}\nMessage: {message}\n"
                         f"Time elaplsed: {str(datetime.now() - start)}\n\n")
            sleep(10)
    else:
        client = session.client(service_name='emr')
        if args.mode == 'standalone':
            cluster_id = start_emr_job(client)
            response = client.describe_cluster(ClusterId=cluster_id)
            status = response['Cluster']['Status']
            name = response['Cluster']['Name']
            start = datetime.now()
            while status.get('State', '') != 'TERMINATED':
                status = get_status(client, cluster_id)
                logging.info(f"Cluster {name} state: {status['State']}. "
                             f"Reason: {status['StateChangeReason']}. "
                             f"Time elaplsed: {str(datetime.now() - start)}")
                if status.get('State', '') == 'TERMINATED_WITH_ERRORS':
                    break
                sleep(10)
        elif args.mode == 'persistent':
            cluster_id, step_ids = post_emr_job(client)
            start = datetime.now()
            job_steps, failed_steps, completed_steps = get_job_steps(client)
            while not failed_steps or len(completed_steps) == len(job_steps):
                logging.info('\n'.join([log_step(step) for step in job_steps]) +
                             f"\nTime elapsed {str(datetime.now() - start)}")
                job_steps, failed_steps, completed_steps = get_job_steps(client)
                sleep(10)
